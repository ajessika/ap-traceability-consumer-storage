package br.com.dasa.aptraceabilityconsumerstorage.repository;

import com.microsoft.azure.storage.blob.CloudAppendBlob;
import com.microsoft.azure.storage.blob.CloudBlobClient;
import com.microsoft.azure.storage.blob.CloudBlobContainer;
import com.microsoft.azure.storage.blob.CloudBlockBlob;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

@Service
public class CloudStorageRepository {

    @Autowired
    private CloudBlobClient blobClient;

    @Value("${azure.storage.containerName}")
    private String containerName;

    private Logger logger = LoggerFactory.getLogger(CloudStorageRepository.class);

    public HttpStatus saveData(String message, String fullName) {
        try {
            logger.info("Saving message into container {} with path {}", containerName, fullName);
            CloudBlobContainer container = blobClient.getContainerReference(containerName);
            CloudAppendBlob blob = container.getAppendBlobReference(fullName);

            if (!blob.exists()) {
                blob.createOrReplace();
            }
            blob.appendText(message + "\n");
            logger.info("Message saved with success in {}", fullName);
        } catch (Exception ex) {
            throw new RuntimeException(ex.getMessage());
        }
        return HttpStatus.CREATED;
        }
   }

//    public HttpStatus saveData(String message, String fullName) {
//        try {
//            CloudBlobContainer container = blobClient.getContainerReference(containerName);
//            System.out.println(containerName);
//            CloudBlockBlob blob = container.getBlockBlobReference(fullName);
//            blob.uploadText(message);
//            System.out.println(message);
//        }catch (Exception ex) {
//            throw new RuntimeException(ex.getMessage());
//        }
//        return HttpStatus.CREATED;
//    }
