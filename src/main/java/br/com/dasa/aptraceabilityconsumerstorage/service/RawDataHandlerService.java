package br.com.dasa.aptraceabilityconsumerstorage.service;

import br.com.dasa.aptraceabilityconsumerstorage.repository.CloudStorageRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Date;

@Service
public class RawDataHandlerService {

    @Value("${rawDataPath}")
    public String rawDataPath;

    @Value("${message.extension}")
    public String extension;

    @Autowired
    private CloudStorageRepository repository;

    private Logger logger = LoggerFactory.getLogger(RawDataHandlerService.class);

    public HttpStatus handleRawMessage(String rawMessage) {
        try {
            logger.debug("This message will be  moved to storage directory {}", rawDataPath);
            return repository.saveData(rawMessage, defineRawFilePath());
        } catch (Exception ex) {
            logger.error("Error trying to save received message to storage", ex);
            throw ex;
        }
    }

    private String defineRawFilePath() {
        return new
                StringBuilder(rawDataPath)
                .append("samplesByLot-")
                .append(new SimpleDateFormat("yyyy-MM-dd").format(new Date()))
                .append(extension)
                .toString();
    }

}
