package br.com.dasa.aptraceabilityconsumerstorage.config;

import com.microsoft.azure.storage.CloudStorageAccount;
import com.microsoft.azure.storage.blob.CloudBlobClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.net.URISyntaxException;
import java.security.InvalidKeyException;

@Configuration
public class AzureConfig {

    @Value("${azure.storage.connectionString}")
    private String storageConnection;

    @Bean
    public CloudBlobClient cloudBlobClient() throws URISyntaxException, InvalidKeyException {
        CloudStorageAccount cloudStorageAccount = CloudStorageAccount.parse(storageConnection);
        return cloudStorageAccount.createCloudBlobClient();
    }

}
