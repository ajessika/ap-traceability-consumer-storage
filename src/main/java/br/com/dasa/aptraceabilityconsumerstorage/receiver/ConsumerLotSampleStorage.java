package br.com.dasa.aptraceabilityconsumerstorage.receiver;

import br.com.dasa.aptraceabilityconsumerstorage.service.RawDataHandlerService;

import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@RabbitListener(queues = "traceability.external.lot-sample-storage")
public class ConsumerLotSampleStorage {

    @Autowired
    RawDataHandlerService rawDataService;

    @RabbitHandler
    public void receive(byte[] message) {
        rawDataService.handleRawMessage(new String(message));
    }

}
